/************************************************************************
* Copyright (C) 2024 Francesco Palumbo <phranz.dev@gmail.com>, Naples Italy
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*************************************************************************/

#include <stdio.h>
#include <fcntl.h>
#include <linux/input.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>

#define DEVS "/proc/bus/input/devices"
#define LIDCLOSED 1
#define LIDOPENED 0

int lid_device(char* dev, size_t s) {
    char buf[BUFSIZ];
    int islid = 0;
    FILE *fp = fopen(DEVS, "r");

    if (!fp) {
        perror(DEVS);
        return 0;
    }
    while (fgets(buf, sizeof(buf), fp)) {
        if (!strncmp("N: Name=\"", buf, 9)) { 
            if (!strncmp("Lid", buf+9, 3)) {
                islid = 1;
            } else {
                islid = 0;
            }
            continue;
        }
        if (islid && !strncmp("H: Handlers=", buf, 12)) {
            size_t i = 0;
            for (size_t j=12; j<strlen(buf); ++i, ++j)
                buf[i] = buf[j];
            buf[i] = 0;
            for (i=0; i<strlen(buf); ++i) {
                if (isspace(buf[i])) {
                    buf[i] = 0;
                    break;
                }
            }
            break;
        }
    }
    fclose(fp);

    if (!islid)
        return 0;

    strncpy(dev, "/dev/input/", s-1);
    strncat(dev, buf, s-strlen(dev)-1);

    return 1;
}

int listen_exec(int fd, const char* ocmd, const char* ccmd) {
    struct input_event evt;
    fd_set rfds;
    int ret;

    while (1) {
        FD_ZERO(&rfds);
        FD_SET(fd, &rfds);

        ret = select(fd+1, &rfds, NULL, NULL, NULL);

        if (ret < 0) {
            perror("select");
            return 0;
        }
        if (ret) {
            if (read(fd, &evt, sizeof(evt)) <= 0)
                continue;
            if (evt.type == EV_SW && evt.code == SW_LID) {
                switch (evt.value) {
                    case LIDCLOSED:
                        if (ccmd && *ccmd)
                            system(ccmd);
                        break;
                    case LIDOPENED:
                        if (ocmd && *ocmd)
                            system(ocmd);
                        break;
                }
            }
        }
    }
    return 1;
}

void help(char** argv) {
    fprintf(stderr, "Usage: %s [<lid-open command>] [<lid-closed command>]\n"
        "Version: " VERSION "\n"
        "License: GPL3\n", argv[0]);
}

int main(int argc, char **argv)
{
    if (argc == 1) {
        help(argv);
        exit(EXIT_FAILURE);
    }
    char dev[BUFSIZ];
    int fd;

    if (!lid_device(dev, BUFSIZ)) {
        fprintf(stderr, "Unable to detect lid device!\n");
        exit(EXIT_FAILURE);
    }
    fd = open(dev, O_RDONLY);
    if (fd < 0) {
        perror(dev);
        exit(EXIT_FAILURE);
    }
    if (!listen_exec(fd, argv[1], argc > 2 ? argv[2] : NULL))
        exit(EXIT_FAILURE);
    close(fd);

    return 0;
}
